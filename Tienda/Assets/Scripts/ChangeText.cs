﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.SqliteClient;


public class ChangeText : MonoBehaviour
{
    public GameObject inventario;

    int cantidadespada;
    int cantidadpistola;
    int cantidadbloques;
    int cantidadmapa;
    int cantidadpocion;
    int cantidadbalas;

    void Start()
    {
        
    }

    public void Update()
    {


        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        string cantidad;

        // MOSTRAR DINERO EN PANTALLA

        string consulta = "SELECT * FROM Dinero";

        SqliteCommand cmd = new SqliteCommand(consulta, conexion);

        SqliteDataReader datos = cmd.ExecuteReader();

        while (datos.Read())
        {
            cantidad = Convert.ToString(datos[0]);

            Text txtMy = GameObject.Find("Siempre/Dinero").GetComponent<Text>();
            txtMy.text = "Dinero: " + cantidad;
        }

        // MOSTRAR BALAS EN PANTALLA

        string consulta2 = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Balas'";

        cmd = new SqliteCommand(consulta2, conexion);

        datos = cmd.ExecuteReader();

        while (datos.Read())
        {
            string cantidad2 = Convert.ToString(datos[0]);

            Text txtMy2 = GameObject.Find("Siempre/Balas").GetComponent<Text>();
            txtMy2.text = "Balas: " + cantidad2;
        }

        conexion.Close();

        // INVENTARIO

        if (inventario.activeInHierarchy == true) // comprueba si hemos activado el inventario desde el otro script.
        {
            // busca un texto en el que escribir la información

            Text txtInventario = GameObject.Find("Mensaje/Inventario").GetComponent<Text>();

            SqliteConnection conexion2 = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
            conexion2.Open();

            //coge cantidad de espadas

            string consultaespada = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Espada'";

            SqliteCommand cmd2 = new SqliteCommand(consultaespada, conexion2);

            cmd2.ExecuteNonQuery();

            SqliteDataReader datos2 = cmd2.ExecuteReader();


            while (datos2.Read())
            {
                
                cantidadespada = Convert.ToInt32(datos2[0]);

               
            }

            //coge cantidad de pistolas

            string consultapistola = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Pistola'";

            cmd2 = new SqliteCommand(consultapistola, conexion2);

            cmd2.ExecuteNonQuery();

            datos2 = cmd2.ExecuteReader();


            while (datos2.Read())
            {

                cantidadpistola = Convert.ToInt32(datos2[0]);

                
            }

            //coge cantidad de bloques

            string consultabloque = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Bloque'";

            cmd2 = new SqliteCommand(consultabloque, conexion2);

            cmd2.ExecuteNonQuery();

            datos2 = cmd2.ExecuteReader();


            while (datos2.Read())
            {
                cantidadbloques = Convert.ToInt32(datos2[0]);
            }

            //coge cantidad de pociones

            string consultapocion = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Poción'";

            cmd2 = new SqliteCommand(consultapocion, conexion2);

            cmd2.ExecuteNonQuery();

            datos2 = cmd2.ExecuteReader();


            while (datos2.Read())
            {
                cantidadpocion = Convert.ToInt32(datos2[0]);
            }

            //coge cantidad de Balas

            string consultabalas = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Balas'";

            cmd2 = new SqliteCommand(consultabalas, conexion2);

            cmd2.ExecuteNonQuery();

            datos2 = cmd2.ExecuteReader();


            while (datos2.Read())
            {

                cantidadbalas = Convert.ToInt32(datos2[0]);

                
            }

            // \r\n HACE UN INTRO

            txtInventario.text = ("Espadas: " + cantidadespada) + 
                ("\r\n Pistolas: " + cantidadpistola) +
                ("\r\n Bloques: " + cantidadbloques) + 
                ("\r\n Pociones: " + cantidadpocion) + 
                ("\r\n Balas: " + cantidadbalas);
                

            conexion2.Close();
        }
        
    }

}
