﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perseguido : MonoBehaviour
{
    //Establecer waypoint.
    public GameObject wayPoint;
    //como de frecuente se refresca la posición del personaje (cada medio segundo)
    private float timer = 0.5f;

    public GameObject camara2;
    public GameObject mirilla;

    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        if (timer <= 0)
        {
            //Cunado llegue a cero se actualiza la posición
            UpdatePosition();
            timer = 0.5f;
        }
    }

    void UpdatePosition()
    {
        //Sctualizar posición
        wayPoint.transform.position = transform.position;
    }

    void OnCollisionEnter(Collision col)
    {
        // si el jugador entra en contacto con el enemigo
        if (col.gameObject.layer == 10)
        {
            //Destruír jugador
            Destroy(gameObject);

            Debug.Log("Has muerto :/");

            //cambiar a cámara 2 (ya que la otra se destruye)
            camara2.SetActive(true);
            //desactivar mirilla
            mirilla.SetActive(false);

        }
    }
}
