﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.SqliteClient;


public class ImpactoEspada : MonoBehaviour
{

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == 10)
        {
            // destruye la espada (después de 5 segundos)
            Destroy(gameObject, 5f);

            // destruye al enemigo (después de 5 segundos)
            Destroy(col.gameObject, 5f);

            SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
            conexion.Open();

            // suma 200 de dinero
            string enemigo = "UPDATE Dinero SET Cantidad = Cantidad + 200";
            SqliteCommand cmddiana = new SqliteCommand(enemigo, conexion);
            cmddiana.ExecuteNonQuery();

            conexion.Close();

            //tras el impacto la espada se convierte en hijo del enemigo, por lo que se unen
            gameObject.transform.parent = col.gameObject.transform;



            Debug.Log("IMPACTOOOOOOOOO");
        }
    }
}
