﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.SqliteClient;

public class ImpactoBala : MonoBehaviour
{
    public bool contar;
    public float contador = 2.0f;
    
    GameObject vendedor;
    



    // Start is called before the first frame update
    void Start()
    {
        // Asignamos el vendedor
        vendedor = GameObject.Find("Vendedor");
    }

    // Update is called once per frame
    void Update()
    {

        

        

        
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == 9) // capa del vendedor
        {
            // desactiva al vendedor
            vendedor.SetActive(false);

            // destruye la bala
            Destroy(gameObject);

            Debug.Log("IMPACTOOOOOOOOO");
        }

        if (col.gameObject.layer == 11) // capa de las dianas
        {
            //desactiva diana
            col.gameObject.SetActive(false);

            // destruye la bala
            Destroy(gameObject);

            Debug.Log("IMPACTOOOOOOOOO + 100");

            SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
            conexion.Open();

            // suma 100 de dinero

            string diana = "UPDATE Dinero SET Cantidad = Cantidad + 100";
            SqliteCommand cmddiana = new SqliteCommand(diana, conexion);
            cmddiana.ExecuteNonQuery();

            conexion.Close();

        }

        if (col.gameObject.layer == 12) // capa de objetos de cristal
        {
            // Destruye objeto de cristal
            Destroy(col.gameObject);

            // destruye la bala
            Destroy(gameObject);

            Debug.Log("IMPACTOOOOOOOOO");

        }

    }
}
