﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.SqliteClient;

public class Test : MonoBehaviour
{
    public float contador = 2f;

    public bool contar = false;

    float enemigos;
    float enemigos2;
    float enemigos3;

    void Start()
    {
        enemigos = 5f;

        enemigos2 = 7.5f;

        enemigos3 = 10f;

        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        string consulta = "SELECT * FROM Items"; // MOSTRAR INFORMACIÓN DE LA TIENDA
        SqliteCommand cmd = new SqliteCommand(consulta, conexion);
        SqliteDataReader datos = cmd.ExecuteReader();

        while (datos.Read())
        {
            string nombre = Convert.ToString(datos[0]);
            int precio = Convert.ToInt32(datos[1]);
            int cantidad = Convert.ToInt32(datos[2]);
            Debug.Log("Nombre: " + nombre + " Precio: " + precio + " Cantidad: " + cantidad);
        }

        string resetdinero = "UPDATE Dinero SET Cantidad = 650"; // APARECER CON 650 DE DINERO SIEMPRE
        cmd = new SqliteCommand(resetdinero, conexion);

        cmd.ExecuteNonQuery();

        Debug.Log("El dinero es 650.");

        string resetinv = "UPDATE Inventario SET Cantidad = 0"; //resetear inventario
        cmd = new SqliteCommand(resetinv, conexion);

        cmd.ExecuteNonQuery();

        conexion.Close();

        Reset();

        // BUSCAR OBJETOS 

        sword = GameObject.Find("FPSPlayer/Main Camera/Sword");

        deagle = GameObject.Find("FPSPlayer/Main Camera/Deagle");

        pocion = GameObject.Find("FPSPlayer/Main Camera/Potion");

        bloque = GameObject.Find("FPSPlayer/Main Camera/Block");

        

        // guardar posición inicial Vendedor

        originalRotationValue = vendedor.transform.rotation;



    }

    public void Reset()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        //No utilizar Items.Cantidad //

        // CAMBIA LA CANTIDAD DE ITEMS RESTANTES AL NÚMERO QUE SE PONGA

        string resettienda = "UPDATE Items SET Cantidad = 10 WHERE Nombre = 'Espada'";
        SqliteCommand cmd2 = new SqliteCommand(resettienda, conexion);

        cmd2.ExecuteNonQuery();

        string resettienda2 = "UPDATE Items SET Cantidad = 1 WHERE Nombre = 'Pistola'";
        cmd2 = new SqliteCommand(resettienda2, conexion);

        cmd2.ExecuteNonQuery();

        string resettienda3 = "UPDATE Items SET Cantidad = 30 WHERE Nombre = 'Bloque'";
        cmd2 = new SqliteCommand(resettienda3, conexion);

        cmd2.ExecuteNonQuery();

        string resettienda4 = "UPDATE Items SET Cantidad = 1 WHERE Nombre = 'Mapa'";
        cmd2 = new SqliteCommand(resettienda4, conexion);

        cmd2.ExecuteNonQuery();

        string resettienda5 = "UPDATE Items SET Cantidad = 20 WHERE Nombre = 'Poción'";
        cmd2 = new SqliteCommand(resettienda5, conexion);

        cmd2.ExecuteNonQuery();

        string resettienda6 = "UPDATE Items SET Cantidad = 20 WHERE Nombre = 'Munición'";
        cmd2 = new SqliteCommand(resettienda6, conexion);

        cmd2.ExecuteNonQuery();


        //RESPAWNEAR AL VENDEDOR


        vendedor.SetActive(true);

        // establecemos lugar de respawn y anulamos el momento (producido por el impacto de la bala)

        vendedor.transform.position = new Vector3(230, 21, 69);
        vendedorRigidbody.velocity = Vector3.zero;
        vendedorRigidbody.angularVelocity = Vector3.zero;

        // igualamos la rotación del vendedor reactivado a la original (usamos quaterniones)
        
        vendedor.transform.rotation = Quaternion.Slerp(vendedor.transform.rotation, originalRotationValue, Time.time * rotationResetSpeed);


        //string resetinventario

        //comando para mostrar en consola en Unity

        Debug.Log("La tabla se ha reseteado");

        conexion.Close();
    }

    public void ResetDinero()
    {

        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        // PONE EL DINERO A 0

        string resetdinero = "UPDATE Dinero SET Cantidad = 0";
        SqliteCommand cmd4 = new SqliteCommand(resetdinero, conexion);

        cmd4.ExecuteNonQuery();

        Debug.Log("El dinero es 0.");

        conexion.Close();

    }

    public void ResetInventario()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        // ASIGNA LA CANTIDAD DE 0 A TODOS LOS ELEMENTOS DEL INVENTARIO

        string resetdinero = "UPDATE Inventario SET Cantidad = 0";
        SqliteCommand cmd4 = new SqliteCommand(resetdinero, conexion);

        cmd4.ExecuteNonQuery();

        Debug.Log("Inventario reseteado.");

        conexion.Close();
    }
    public void Dinero()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        //No utilizar Items.Cantidad

        // AUMENTA 2000 AL DINERO ACTUAL

        string dinero = "UPDATE Dinero SET Cantidad = Cantidad + 2000 ";
        SqliteCommand cmd3 = new SqliteCommand(dinero, conexion);

        cmd3.ExecuteNonQuery();



        string consultadinero = "SELECT Cantidad FROM Dinero";

        cmd3 = new SqliteCommand(consultadinero, conexion);
        SqliteDataReader datos = cmd3.ExecuteReader();

        while (datos.Read())
        {
            int total = Convert.ToInt32(datos[0]);

            // MUESTRA EL CAMBIO EN CONSOLA

            Debug.Log("2000 de dinero han sido añadidos al total. Total: " + total);
        }

        conexion.Close();
    }

    // TIENDAAA

    public void Espada()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        string haydinero = "SELECT Cantidad FROM Dinero";

        SqliteCommand cmd5 = new SqliteCommand(haydinero, conexion);

        cmd5.ExecuteNonQuery();

        SqliteDataReader datosdinero = cmd5.ExecuteReader();

        while (datosdinero.Read())
        {
            int dinero = Convert.ToInt32(datosdinero[0]);

            // COMPRUEBA SI AL RESTAR EL PRECIO DEL PRODUCTO AL TOTAL DE DINERO DA NEGATIVO, SIGNIFICANDO QUE EL DINERO NO ES SUFICIENTE

            if ((dinero - 75) < 0)
            {
                Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                txtMy.text = "No hay dinero suficiente";

                contar = true;
            }

            // SI TENEMOS SUFICIENTE DINERO

            if ((dinero - 75) >= 0)
            {
                string cantidad = "SELECT Cantidad FROM Items WHERE Nombre = 'Espada'";

                cmd5 = new SqliteCommand(cantidad, conexion);

                cmd5.ExecuteNonQuery();

                SqliteDataReader datos = cmd5.ExecuteReader();

                while (datos.Read())
                {
                    int restantes = Convert.ToInt32(datos[0]);

                    if (restantes <= 0)
                    {
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Se han agotado las espadas";


                        contar = true;
                    }

                    if (restantes >= 1)
                    {
                        string comprarespada = "UPDATE Items SET Cantidad = Cantidad - 1 WHERE Nombre = 'Espada'";

                        cmd5 = new SqliteCommand(comprarespada, conexion);

                        cmd5.ExecuteNonQuery();

                        // RESTA EL PRECIO DEL ITEM

                        string restardinero = "UPDATE Dinero SET Cantidad = Cantidad - 75";

                        cmd5 = new SqliteCommand(restardinero, conexion);

                        cmd5.ExecuteNonQuery();

                        //Comprobar si ya está en el inventario 

                        string existe = "SELECT count(Nombre) FROM Inventario WHERE Nombre = 'Espada'";
                        cmd5 = new SqliteCommand(existe, conexion);
                        cmd5.ExecuteNonQuery();

                        SqliteDataReader datos2 = cmd5.ExecuteReader();

                        while (datos2.Read())
                        {
                            int total = Convert.ToInt32(datos2[0]);

                            //SI YA EXISTE

                            if (total > 0)
                            {
                                string añadirespada = "UPDATE Inventario SET Cantidad = Cantidad + 1 WHERE Nombre = 'Espada'";

                                cmd5 = new SqliteCommand(añadirespada, conexion);

                                cmd5.ExecuteNonQuery();
                            }

                            //SI NO EXISTE

                            if (total == 0)
                            {
                                string añadirespada = "INSERT INTO Inventario VALUES ('Espada', 1)";

                                cmd5 = new SqliteCommand(añadirespada, conexion);

                                cmd5.ExecuteNonQuery();
                            }
                        }

                        // Muestra mensaje en consola

                        Debug.Log("Espada comprada.");

                        // Muestra mensaje en pantalla

                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Espada comprada";

                        // Activa un contador de 2 segundos (void update), el cual cambia el texto del mensaje a "" cuando termina.

                        contar = true;
                    }
                }


            }

        }
        conexion.Close();
    }

    public void Pistola()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        string haydinero = "SELECT Cantidad FROM Dinero";

        SqliteCommand cmd6 = new SqliteCommand(haydinero, conexion);

        cmd6.ExecuteNonQuery();

        SqliteDataReader datosdinero = cmd6.ExecuteReader();

        while (datosdinero.Read())
        {
            int dinero = Convert.ToInt32(datosdinero[0]);

            if ((dinero - 500) < 0)
            {
                Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                txtMy.text = "No hay dinero suficiente";

                contar = true;
            }

            if ((dinero - 500) >= 0)
            {
                string cantidad = "SELECT Cantidad FROM Items WHERE Nombre = 'Pistola'";

                cmd6 = new SqliteCommand(cantidad, conexion);

                cmd6.ExecuteNonQuery();

                SqliteDataReader datos = cmd6.ExecuteReader();

                while (datos.Read())
                {
                    int restantes = Convert.ToInt32(datos[0]);

                    if (restantes <= 0)
                    {
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Se han agotado las pistolas";


                        contar = true;
                    }

                    if (restantes >= 1)
                    {
                        string comprarpistola = "UPDATE Items SET Cantidad = Cantidad - 1 WHERE Nombre = 'Pistola'";
                        cmd6 = new SqliteCommand(comprarpistola, conexion);

                        cmd6.ExecuteNonQuery();

                        string restardinero = "UPDATE Dinero SET Cantidad = Cantidad - 500";

                        cmd6 = new SqliteCommand(restardinero, conexion);

                        cmd6.ExecuteNonQuery();

                        //Comprobar si ya está en el inventario 
                        string existe = "SELECT count(Nombre) FROM Inventario WHERE Nombre = 'Pistola'";
                        cmd6 = new SqliteCommand(existe, conexion);
                        cmd6.ExecuteNonQuery();

                        SqliteDataReader datos2 = cmd6.ExecuteReader();

                        while (datos2.Read())
                        {
                            int total = Convert.ToInt32(datos2[0]);

                            if (total > 0)
                            {
                                string añadirpistola = "UPDATE Inventario SET Cantidad = Cantidad + 1 WHERE Nombre = 'Pistola'";

                                cmd6 = new SqliteCommand(añadirpistola, conexion);

                                cmd6.ExecuteNonQuery();
                            }

                            if (total == 0)
                            {
                                string añadirpistola = "INSERT INTO Inventario VALUES ('Pistola', 1)";

                                cmd6 = new SqliteCommand(añadirpistola, conexion);

                                cmd6.ExecuteNonQuery();
                            }
                        }



                        Debug.Log("Pistola comprada.");
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Pistola comprada";

                        contar = true;
                    }

                }

            }
        }
        conexion.Close();



    }

    public void Bloques()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();

        string haydinero = "SELECT Cantidad FROM Dinero";

        SqliteCommand cmd7 = new SqliteCommand(haydinero, conexion);

        cmd7.ExecuteNonQuery();

        SqliteDataReader datosdinero = cmd7.ExecuteReader();

        while (datosdinero.Read())
        {
            int dinero = Convert.ToInt32(datosdinero[0]);

            if ((dinero - 30) < 0)
            {
                Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                txtMy.text = "No hay dinero suficiente";

                contar = true;
            }

            if ((dinero - 30) >= 0)
            {
                string cantidad = "SELECT Cantidad FROM Items WHERE Nombre = 'Bloque'";

                cmd7 = new SqliteCommand(cantidad, conexion);

                cmd7.ExecuteNonQuery();

                SqliteDataReader datos = cmd7.ExecuteReader();

                while (datos.Read())
                {
                    int restantes = Convert.ToInt32(datos[0]);

                    if (restantes <= 0)
                    {
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Se han agotado los bloques";


                        contar = true;
                    }

                    if (restantes >= 1)
                    {
                        string comprarbloque = "UPDATE Items SET Cantidad = Cantidad - 1 WHERE Nombre = 'Bloque'";
                        cmd7 = new SqliteCommand(comprarbloque, conexion);

                        cmd7.ExecuteNonQuery();

                        string restardinero = "UPDATE Dinero SET Cantidad = Cantidad - 30";

                        cmd7 = new SqliteCommand(restardinero, conexion);

                        cmd7.ExecuteNonQuery();

                        //Comprobar si ya está en el inventario 
                        string existe = "SELECT count(Nombre) FROM Inventario WHERE Nombre = 'Bloque'";
                        cmd7 = new SqliteCommand(existe, conexion);
                        cmd7.ExecuteNonQuery();

                        SqliteDataReader datos2 = cmd7.ExecuteReader();

                        while (datos2.Read())
                        {
                            int total = Convert.ToInt32(datos2[0]);

                            if (total > 0)
                            {
                                string añadirbloque = "UPDATE Inventario SET Cantidad = Cantidad + 1 WHERE Nombre = 'Bloque'";

                                cmd7 = new SqliteCommand(añadirbloque, conexion);

                                cmd7.ExecuteNonQuery();
                            }

                            if (total == 0)
                            {
                                string añadirbloque = "INSERT INTO Inventario VALUES ('Bloque', 1)";

                                cmd7 = new SqliteCommand(añadirbloque, conexion);

                                cmd7.ExecuteNonQuery();
                            }
                        }

                        

                        Debug.Log("Bloque comprado.");
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Bloque comprado";

                        contar = true;
                    }




                }
            }
        }
        conexion.Close();
               

    }

    public void Mapa()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();


        string haydinero = "SELECT Cantidad FROM Dinero";

        SqliteCommand cmd8 = new SqliteCommand(haydinero, conexion);

        cmd8.ExecuteNonQuery();

        SqliteDataReader datosdinero = cmd8.ExecuteReader();

        while (datosdinero.Read())
        {
            int dinero = Convert.ToInt32(datosdinero[0]);

            if ((dinero - 300) < 0)
            {
                Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                txtMy.text = "No hay dinero suficiente";

                contar = true;
            }

            if ((dinero - 300) >= 0)
            {
                string cantidad = "SELECT Cantidad FROM Items WHERE Nombre = 'Mapa'";

                cmd8 = new SqliteCommand(cantidad, conexion);

                cmd8.ExecuteNonQuery();

                SqliteDataReader datos = cmd8.ExecuteReader();

                while (datos.Read())
                {
                    int restantes = Convert.ToInt32(datos[0]);

                    if (restantes <= 0)
                    {
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Se han agotado los mapas";


                        contar = true;
                    }

                    if (restantes >= 1)
                    {
                        string comprarmapa = "UPDATE Items SET Cantidad = Cantidad - 1 WHERE Nombre = 'Mapa'";
                        cmd8 = new SqliteCommand(comprarmapa, conexion);

                        cmd8.ExecuteNonQuery();

                        string restardinero = "UPDATE Dinero SET Cantidad = Cantidad - 300";

                        cmd8 = new SqliteCommand(restardinero, conexion);

                        cmd8.ExecuteNonQuery();

                        //Comprobar si ya está en el inventario 
                        string existe = "SELECT count(Nombre) FROM Inventario WHERE Nombre = 'Mapa'";
                        cmd8 = new SqliteCommand(existe, conexion);
                        cmd8.ExecuteNonQuery();

                        SqliteDataReader datos2 = cmd8.ExecuteReader();

                        while (datos2.Read())
                        {
                            int total = Convert.ToInt32(datos2[0]);

                            if (total > 0)
                            {
                                string añadirmapa = "UPDATE Inventario SET Cantidad = Cantidad + 1 WHERE Nombre = 'Mapa'";

                                cmd8 = new SqliteCommand(añadirmapa, conexion);

                                cmd8.ExecuteNonQuery();
                            }

                            if (total == 0)
                            {
                                string añadirmapa = "INSERT INTO Inventario VALUES ('Mapa', 1)";

                                cmd8 = new SqliteCommand(añadirmapa, conexion);

                                cmd8.ExecuteNonQuery();
                            }
                        }


                        Debug.Log("Mapa comprado.");
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Mapa comprado";

                        contar = true;

                    }
                }

            }
        }
        conexion.Close();






    }

    public void Pocion()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();


        string haydinero = "SELECT Cantidad FROM Dinero";

        SqliteCommand cmd9 = new SqliteCommand(haydinero, conexion);

        cmd9.ExecuteNonQuery();

        SqliteDataReader datosdinero = cmd9.ExecuteReader();

        while (datosdinero.Read())
        {
            int dinero = Convert.ToInt32(datosdinero[0]);

            if ((dinero - 20) < 0)
            {
                Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                txtMy.text = "No hay dinero suficiente";

                contar = true;
            }

            if ((dinero - 20) >= 0)
            {
                string cantidad = "SELECT Cantidad FROM Items WHERE Nombre = 'Poción'";

                cmd9 = new SqliteCommand(cantidad, conexion);

                cmd9.ExecuteNonQuery();

                SqliteDataReader datos = cmd9.ExecuteReader();

                while (datos.Read())
                {
                    int restantes = Convert.ToInt32(datos[0]);

                    if (restantes <= 0)
                    {
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Se han agotado las pociones";


                        contar = true;
                    }

                    if (restantes >= 1)
                    {
                        string comprarpocion = "UPDATE Items SET Cantidad = Cantidad - 1 WHERE Nombre = 'Poción'";

                        cmd9 = new SqliteCommand(comprarpocion, conexion);

                        cmd9.ExecuteNonQuery();

                        string restardinero = "UPDATE Dinero SET Cantidad = Cantidad - 20";

                        cmd9 = new SqliteCommand(restardinero, conexion);

                        cmd9.ExecuteNonQuery();

                        //Comprobar si ya está en el inventario 
                        string existe = "SELECT count(Nombre) FROM Inventario WHERE Nombre = 'Espada'";
                        cmd9 = new SqliteCommand(existe, conexion);
                        cmd9.ExecuteNonQuery();

                        SqliteDataReader datos2 = cmd9.ExecuteReader();

                        while (datos2.Read())
                        {
                            int total = Convert.ToInt32(datos2[0]);

                            if (total > 0)
                            {
                                string añadirpocion = "UPDATE Inventario SET Cantidad = Cantidad + 1 WHERE Nombre = 'Poción'";

                                cmd9 = new SqliteCommand(añadirpocion, conexion);

                                cmd9.ExecuteNonQuery();
                            }

                            if (total == 0)
                            {
                                string añadirpocion = "INSERT INTO Inventario VALUES ('Poción', 1)";

                                cmd9 = new SqliteCommand(añadirpocion, conexion);

                                cmd9.ExecuteNonQuery();
                            }
                        }
                        

                        Debug.Log("Poción comprada.");
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Poción comprada";

                        contar = true;
                    }

                }
            }
        }

        conexion.Close();




    }

    public void Municion()
    {
        SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
        conexion.Open();


        string haydinero = "SELECT Cantidad FROM Dinero";

        SqliteCommand cmd10 = new SqliteCommand(haydinero, conexion);

        cmd10.ExecuteNonQuery();

        SqliteDataReader datosdinero = cmd10.ExecuteReader();

        while (datosdinero.Read())
        {
            int dinero = Convert.ToInt32(datosdinero[0]);

            if ((dinero - 20) < 0)
            {
                Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                txtMy.text = "No hay dinero suficiente";

                contar = true;
            }

            if ((dinero - 20) >= 0)
            {
                string cantidad = "SELECT Cantidad FROM Items WHERE Nombre = 'Munición'";

                cmd10 = new SqliteCommand(cantidad, conexion);

                cmd10.ExecuteNonQuery();

                SqliteDataReader datos = cmd10.ExecuteReader();

                while (datos.Read())
                {
                    int restantes = Convert.ToInt32(datos[0]);

                    if (restantes <= 0)
                    {
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Se han agotado las cajas de munición";


                        contar = true;
                    }

                    if (restantes >= 1)
                    {
                        string comprarmunicion = "UPDATE Items SET Cantidad = Cantidad - 1 WHERE Nombre = 'Munición'";
                        cmd10 = new SqliteCommand(comprarmunicion, conexion);

                        cmd10.ExecuteNonQuery();

                        string restardinero = "UPDATE Dinero SET Cantidad = Cantidad - 20";

                        cmd10 = new SqliteCommand(restardinero, conexion);

                        cmd10.ExecuteNonQuery();

                        //Comprobar si ya está en el inventario 
                        string existe = "SELECT count(Nombre) FROM Inventario WHERE Nombre = 'Balas'";
                        cmd10 = new SqliteCommand(existe, conexion);
                        cmd10.ExecuteNonQuery();

                        SqliteDataReader datos2 = cmd10.ExecuteReader();

                        while (datos2.Read())
                        {
                            int total = Convert.ToInt32(datos2[0]);

                            if (total > 0)
                            {
                                string añadirmunicion = "UPDATE Inventario SET Cantidad = Cantidad + 10 WHERE Nombre = 'Balas'";

                                cmd10 = new SqliteCommand(añadirmunicion, conexion);

                                cmd10.ExecuteNonQuery();
                            }

                            if (total == 0)
                            {
                                string añadirmunicion = "INSERT INTO Inventario VALUES ('Balas', 10)";

                                cmd10 = new SqliteCommand(añadirmunicion, conexion);

                                cmd10.ExecuteNonQuery();
                            }
                        }


                        Debug.Log("Munición comprada.");
                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Munición comprada";

                        contar = true;
                    }
                }
            }
        }
        conexion.Close();








    }
    
    // INSTANCIANDOOOOOOOOOOOOOOOOOOOOOOOOO

    public GameObject spawnPoint;
    public GameObject ExplosionPrefab;
    public Rigidbody projectilePrefab;
    public GameObject mapaImagen;

    public GameObject spawnPointBloque;
    public Rigidbody bloquePrefab;

    public GameObject spawnPointEnemigos;
    public GameObject spawnPointEnemigos2;
    public GameObject spawnPointEnemigos3;
    public Rigidbody enemigoPrefab;

    public Rigidbody espadaRigidbody;

    GameObject sword;
    GameObject deagle;
    GameObject pocion;
    GameObject bloque;
    


    public GameObject vendedor;
    public Rigidbody vendedorRigidbody;

    public Quaternion originalRotationValue; // declare this as a Quaternion

    float rotationResetSpeed = 1.0f;

    float contadorpocion = 7.0f;
    bool contarpocion = false;

    public GameObject inventario;





    public void Update()
    {
        // DESAPARECER MENSAJES EN PANTALLA (contador de 2 segundos)

        if (contar == true)
        {
            contador -= Time.deltaTime;
        }


        if (contador <= 0)
        {
            Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
            txtMy.text = "";
            contar = false;
            contador = 2;
        }

        // COMPROBAR SI ESTÁS CON EL CURSOR BLOQUEADO, SINO NO DISPARA NI ACTIVA/DESACTIVA ARMAS

        if (Cursor.lockState == CursorLockMode.Locked)
        {
            // ACTIVAR ESPADA

            if (Input.GetKeyUp(KeyCode.H))
            {
                SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                conexion.Open();

                string hay = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Espada'";
                SqliteCommand cmd5 = new SqliteCommand(hay, conexion);
                cmd5.ExecuteNonQuery();

                SqliteDataReader datos = cmd5.ExecuteReader();

                while (datos.Read())
                {
                    int total = Convert.ToInt32(datos[0]);

                    // COMPROBAR SI HAY ESPADAS EN EL INVENTARIO

                    if (total > 0)
                    {

                        // si pongo sword.SetActive(false) dentro del if para comprobar no funciona

                        if (sword.activeInHierarchy == true)
                        {
                            sword.SetActive(false);
                        }

                        else
                        {
                            sword.SetActive(true);

                            // DESACTIVAR EL RESTO DE COSAS

                            pocion.SetActive(false);
                            deagle.SetActive(false);
                            mapaImagen.SetActive(false);
                            bloque.SetActive(false);
                        }



                    }

                    if (total <= 0)
                    {
                        Text txtMy3 = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy3.text = "No tienes una espada...";

                        contar = true;
                    }


                }
                conexion.Close();
            }

            // ACTIVAR PISTOLA

            if (Input.GetKeyUp(KeyCode.P))
            {
                SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                conexion.Open();

                string hay = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Pistola'";
                SqliteCommand cmdpistola = new SqliteCommand(hay, conexion);
                cmdpistola.ExecuteNonQuery();

                SqliteDataReader datos = cmdpistola.ExecuteReader();

                while (datos.Read())
                {
                    int total = Convert.ToInt32(datos[0]);

                    if (total > 0)
                    {

                        // si pongo sword.SetActive(false) dentro del if para comprobar no funciona ya que eso solo vale para cambiar el estado del objeto, así que comprobamos si está activo en la jerarquía

                        if (deagle.activeInHierarchy == true)
                        {
                            deagle.SetActive(false);
                        }

                        else
                        {
                            deagle.SetActive(true);

                            // DESACTIVAR EL RESTO DE COSAS

                            sword.SetActive(false);
                            pocion.SetActive(false);
                            mapaImagen.SetActive(false);
                            bloque.SetActive(false);
                        }



                    }

                    if (total <= 0)
                    {
                        Text txtMy3 = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy3.text = "No tienes una pistola...";

                        contar = true;
                    }


                }
                conexion.Close();
            }

            // ACTIVAR POCIÓN

            if (Input.GetKeyUp(KeyCode.O))
            {
                SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                conexion.Open();

                string hay = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Poción'";
                SqliteCommand cmdpocion = new SqliteCommand(hay, conexion);
                cmdpocion.ExecuteNonQuery();

                SqliteDataReader datos = cmdpocion.ExecuteReader();

                while (datos.Read())
                {
                    int total = Convert.ToInt32(datos[0]);

                    if (total > 0)
                    {

                        // si pongo sword.SetActive(false) dentro del if para comprobar no funciona ya que eso solo vale para cambiar el estado del objeto, así que comprobamos si está activo en la jerarquía

                        if (pocion.activeInHierarchy == true)
                        {
                            pocion.SetActive(false);
                        }

                        else
                        {
                            pocion.SetActive(true);

                            // DESACTIVAR EL RESTO DE COSAS

                            sword.SetActive(false);
                            deagle.SetActive(false);
                            mapaImagen.SetActive(false);
                            bloque.SetActive(false);


                        }



                    }

                    if (total <= 0)
                    {
                        Text txtMy3 = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy3.text = "No tienes pociones...";

                        contar = true;
                    }


                }
                conexion.Close();
            }

            // ACTIVAR MAPA

            if (Input.GetKeyUp(KeyCode.M))
            {
                SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                conexion.Open();

                string hay = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Mapa'";
                SqliteCommand cmdmapa = new SqliteCommand(hay, conexion);
                cmdmapa.ExecuteNonQuery();

                SqliteDataReader datos = cmdmapa.ExecuteReader();

                while (datos.Read())
                {
                    int total = Convert.ToInt32(datos[0]);

                    if (total > 0)
                    {

                        // si pongo sword.SetActive(false) dentro del if para comprobar no funciona ya que eso solo vale para cambiar el estado del objeto, así que comprobamos si está activo en la jerarquía

                        if (mapaImagen.activeInHierarchy == true)
                        {
                            mapaImagen.SetActive(false);

                        }

                        else
                        {
                            mapaImagen.SetActive(true);


                            // DESACTIVAR EL RESTO DE COSAS

                            sword.SetActive(false);
                            deagle.SetActive(false);
                            pocion.SetActive(false);
                            bloque.SetActive(false);


                        }



                    }

                    if (total <= 0)
                    {
                        Text txtMy3 = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy3.text = "No tienes un mapa...";

                        contar = true;
                    }


                }
                conexion.Close();
            }

            // ACTIVAR BLOQUES

            if (Input.GetKeyUp(KeyCode.B))
            {
                SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                conexion.Open();

                string hay = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Bloque'";
                SqliteCommand cmdbloque = new SqliteCommand(hay, conexion);
                cmdbloque.ExecuteNonQuery();

                SqliteDataReader datos = cmdbloque.ExecuteReader();

                while (datos.Read())
                {
                    int total = Convert.ToInt32(datos[0]);

                    if (total > 0)
                    {

                        // si pongo sword.SetActive(false) dentro del if para comprobar no funciona ya que eso solo vale para cambiar el estado del objeto, así que comprobamos si está activo en la jerarquía

                        if (bloque.activeInHierarchy == true)
                        {
                            bloque.SetActive(false);
                        }

                        else
                        {
                            bloque.SetActive(true);

                            // DESACTIVAR EL RESTO DE COSAS

                            sword.SetActive(false);
                            deagle.SetActive(false);
                            mapaImagen.SetActive(false);
                            pocion.SetActive(false);


                        }



                    }

                    if (total <= 0)
                    {
                        Text txtMy3 = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy3.text = "No tienes bloques...";

                        contar = true;
                    }


                }
                conexion.Close();
            }




            // DISPARAR Y COMPROBRAR MUNICIÓN

            if (Input.GetButtonDown("Fire1") && (deagle.activeInHierarchy == true))
            {
                if (deagle.activeInHierarchy == true)
                {
                    SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                    conexion.Open();

                    //CUENTA LAS BALAS QUE HAY

                    string haybalas = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Balas'";

                    SqliteCommand cmd = new SqliteCommand(haybalas, conexion);
                    cmd.ExecuteNonQuery();

                    SqliteDataReader datos = cmd.ExecuteReader();
                    while (datos.Read())
                    {
                        int balas = Convert.ToInt32(datos[0]);

                        // SI HAY BALAS

                        if (balas > 0)
                        {

                            string restarbalas = "UPDATE Inventario SET Cantidad = Cantidad - 1 WHERE Nombre = 'Balas'";

                            cmd = new SqliteCommand(restarbalas, conexion);
                            cmd.ExecuteNonQuery();

                            Rigidbody hitPlayer;

                             // Instantiate(elementoQueDispara, lugarDeSpawn, rotaciónDeSpawn)

                            hitPlayer = Instantiate(projectilePrefab, spawnPoint.transform.position, spawnPoint.transform.rotation) as Rigidbody;

                            // ESTABLECE LA VELOCIDAD A LA QUE SE DISPARA

                            hitPlayer.velocity = transform.TransformDirection(Vector3.forward * 200);
                        }

                        // SI NO QUEDAN BALAS

                        if (balas <= 0)
                        {
                            // POR SI ACASO LOGRA BAJAR DE 0 LO VUELVE A PONER A 0

                            string cerobalas = "UPDATE Inventario SET Cantidad = 0 WHERE Nombre = 'Balas'";

                            cmd = new SqliteCommand(cerobalas, conexion);
                            cmd.ExecuteNonQuery();


                            Text txtMy3 = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                            txtMy3.text = "No hay Munición...";

                            contar = true;

                        }
                    }

                    conexion.Close();
                }

            }

            // USAR POCIONES 

            if (Input.GetButtonDown("Fire1") && (pocion.activeInHierarchy == true))
            {
                SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                conexion.Open();

                string haypocion = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Poción'";

                SqliteCommand cmd = new SqliteCommand(haypocion, conexion);

                SqliteDataReader datos = cmd.ExecuteReader();
                while (datos.Read())
                {
                    int total = Convert.ToInt32(datos[0]);

                    // UTILIZO 1 PARA QUE SI QUEDA 1 POCIÓN Y LA USO EL OBJETO DESAPAREZCA

                    if (total <= 1)
                    {

                        string restarpocion = "UPDATE Inventario SET Cantidad = Cantidad - 1 WHERE Nombre = 'Poción'";

                        cmd = new SqliteCommand(restarpocion, conexion);

                        cmd.ExecuteNonQuery();

                        // DESACTIVAR OBJETO YA QUE AHORA QUEDAN 0

                        pocion.SetActive(false);

                        SC_FPSController.jumpSpeed = 65;
                        SC_FPSController.gravity = 30;

                        contarpocion = true;

                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Has usado una poción";

                        contar = true;

                    }

                    // SI HAY MÁS DE UNA POCIÓN

                    else if (total > 1)
                    {
                        string restarpocion = "UPDATE Inventario SET Cantidad = Cantidad - 1 WHERE Nombre = 'Poción'";

                        cmd = new SqliteCommand(restarpocion, conexion);

                        cmd.ExecuteNonQuery();

                        // VARIABLES STATIC DE OTRO SCRIPT, HACEN REFERENCIA A LA VELOCIDAD DE SALTO Y A LA GRAVEDAD, CUANDO TOMAS LA POCIÓN ESTAS CAMBIAN

                        SC_FPSController.jumpSpeed = 65.0f;
                        SC_FPSController.gravity = 30.0f;

                        // CONTADOR PARA LA POCIÓN, CUANDO ACABA LA GRAVEDAD Y VELOCIDAD VUELVEN A LA NORMALIDAD

                        contarpocion = true;

                        Text txtMy = GameObject.Find("Mensaje/Text").GetComponent<Text>();
                        txtMy.text = "Has usado una poción";

                        contar = true;
                    }

                }
                conexion.Close();
            }

            // EFECTO DE POCIÓN DURA 7 SEGUNDOS

            if (contarpocion == true)
            {
                contadorpocion -= Time.deltaTime;

                if (contadorpocion <= 0)
                {

                    // RESET GRAVEDAD Y VELOCIDAD DE SALTO

                    SC_FPSController.jumpSpeed = 20.0f;
                    SC_FPSController.gravity = 50.0f;

                    contarpocion = false;
                    contadorpocion = 7.0f;
                }
            }

            // USAR BLOQUES

            if (Input.GetButtonDown("Fire1") && (bloque.activeInHierarchy == true))
            {

                SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                conexion.Open();


                string haybloques = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Bloque'";

                SqliteCommand cmd = new SqliteCommand(haybloques, conexion);
                cmd.ExecuteNonQuery();

                SqliteDataReader datos = cmd.ExecuteReader();
                while (datos.Read())
                {
                    int bloques = Convert.ToInt32(datos[0]);

                    // SI HAY MÁS DE UN BLOQUE

                    if (bloques > 1)
                    {

                        string restarbloques = "UPDATE Inventario SET Cantidad = Cantidad - 1 WHERE Nombre = 'Bloque'";

                        cmd = new SqliteCommand(restarbloques, conexion);
                        cmd.ExecuteNonQuery();

                        Rigidbody hitPlayer;
                        hitPlayer = Instantiate(bloquePrefab, spawnPointBloque.transform.position, spawnPoint.transform.rotation) as Rigidbody;
                        
                    }

                    // UTILIZO 1 PARA QUE SI QUEDA 1 BLOQUE Y LO USO EL OBJETO DESAPAREZCA

                    if (bloques <= 1)
                    {
                        string cerobloques = "UPDATE Inventario SET Cantidad = 0 WHERE Nombre = 'Bloque'";

                        cmd = new SqliteCommand(cerobloques, conexion);
                        cmd.ExecuteNonQuery();

                        Rigidbody cubitos;
                        cubitos = Instantiate(bloquePrefab, spawnPointBloque.transform.position, spawnPointBloque.transform.rotation) as Rigidbody;

                        // DESACTIVAR BLOQUES YA QUE HAY 0 AHORA

                        bloque.SetActive(false);

                    }
                }

                conexion.Close();

            }
            
            // USAR ESPADA

            if (Input.GetButtonDown("Fire1") && (sword.activeInHierarchy == true))
            {
                if (sword.activeInHierarchy == true)
                {
                    SqliteConnection conexion = new SqliteConnection("URI=file:" + Application.dataPath + "/Plugins/Tienda.sqlite");
                    conexion.Open();


                    string hayespadas = "SELECT Cantidad FROM Inventario WHERE Nombre = 'Espada'";

                    SqliteCommand cmd = new SqliteCommand(hayespadas, conexion);
                    cmd.ExecuteNonQuery();

                    SqliteDataReader datos = cmd.ExecuteReader();
                    while (datos.Read())
                    {
                        int espadas = Convert.ToInt32(datos[0]);

                        // SI HAY MÁS DE UNA ESPADA

                        if (espadas > 1)
                        {

                            string restarespadas = "UPDATE Inventario SET Cantidad = Cantidad - 1 WHERE Nombre = 'Espada'";

                            cmd = new SqliteCommand(restarespadas, conexion);
                            cmd.ExecuteNonQuery();

                            Rigidbody Espadita;

                            Espadita = Instantiate(espadaRigidbody, spawnPointBloque.transform.position, espadaRigidbody.transform.rotation) as Rigidbody;
                            Espadita.velocity = transform.TransformDirection(Vector3.forward * 200);
                        }

                        // UTILIZO 1 PARA QUE SI QUEDA 1 ESPADA Y LA USO EL OBJETO DESAPAREZCA

                        if (espadas <= 1)
                        {
                            string ceroespadas = "UPDATE Inventario SET Cantidad = 0 WHERE Nombre = 'Espada'";

                            cmd = new SqliteCommand(ceroespadas, conexion);
                            cmd.ExecuteNonQuery();

                            Rigidbody Espadita;

                            Espadita = Instantiate(espadaRigidbody, spawnPointBloque.transform.position, espadaRigidbody.transform.rotation) as Rigidbody;
                            Espadita.velocity = transform.TransformDirection(Vector3.forward * 200);

                            // DESACTIVO ESPADA

                            sword.SetActive(false);

                        }
                    }

                    conexion.Close();
                }

            }

            if (Input.GetKeyUp(KeyCode.I))
            {
                if (inventario.activeInHierarchy == false)
                {
                    inventario.SetActive(true);
                }

                else if (inventario.activeInHierarchy == true)
                {
                    inventario.SetActive(false);
                }


            }

            




        }

        Rigidbody hitEnemigos;
        Rigidbody hitEnemigos2;
        Rigidbody hitEnemigos3;

        if (enemigos >= -1)
        {
            enemigos -= Time.deltaTime;

            if (enemigos <= 0)
            {
                hitEnemigos = Instantiate(enemigoPrefab, spawnPointEnemigos.transform.position, spawnPointEnemigos.transform.rotation) as Rigidbody;
                enemigos = 5f;
            }
            
        }
        if (enemigos2 >= -1)
        {
            enemigos2 -= Time.deltaTime;

            if (enemigos2 <= 0)
            {
                hitEnemigos2 = Instantiate(enemigoPrefab, spawnPointEnemigos2.transform.position, spawnPointEnemigos2.transform.rotation) as Rigidbody;

                enemigos2 = 7.5f;
            }
            
            
        }
        if (enemigos3 >= -1)
        {
            enemigos3 -= Time.deltaTime;

            if (enemigos3 <= 0)
            {
                hitEnemigos3 = Instantiate(enemigoPrefab, spawnPointEnemigos3.transform.position, spawnPointEnemigos3.transform.rotation) as Rigidbody;

                enemigos3 = 10f;
            }
        }













    }

   

    


}
