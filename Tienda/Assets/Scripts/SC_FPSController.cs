﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.SqliteClient;

[RequireComponent(typeof(CharacterController))]



public class SC_FPSController : MonoBehaviour
{
    public float walkingSpeed = 7.5f;
    public float runningSpeed = 11.5f;
    public static float jumpSpeed = 20.0f;
    public static float gravity = 50.0f;
    public Camera playerCamera;
    public float lookSpeed = 2.0f;
    public float lookXLimit = 45.0f;
    public GameObject inventario;

    //CERRAR TIENDA AL MOVERTE 

    public GameObject tienda;
    public GameObject abrir;
    public GameObject cerrar;

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    float rotationX = 0;

    [HideInInspector]
    public bool canMove = true;

    void Start()
    {
        //asignamos el objeto que tenga ese componente

        characterController = GetComponent<CharacterController>();

        // Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;

        
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.A))
        //{
            
        //    Cursor.lockState = CursorLockMode.None;
        //}

        // SI SE MUEVE SE BLOQUEA

        if (Input.GetKeyDown(KeyCode.W))
        {

            Cursor.lockState = CursorLockMode.Locked;

            inventario.SetActive(false);

            if (tienda.activeInHierarchy == true)
            {
                tienda.SetActive(false);
                abrir.SetActive(true);
                cerrar.SetActive(false);

            }
        }
        if (Input.GetKeyDown(KeyCode.A))
        {

            Cursor.lockState = CursorLockMode.Locked;

            inventario.SetActive(false);

            if (tienda.activeInHierarchy == true)
            {
                tienda.SetActive(false);
                abrir.SetActive(true);
                cerrar.SetActive(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.S))
        {

            Cursor.lockState = CursorLockMode.Locked;

            inventario.SetActive(false);

            if (tienda.activeInHierarchy == true)
            {
                tienda.SetActive(false);
                abrir.SetActive(true);
                cerrar.SetActive(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.D))
        {

            Cursor.lockState = CursorLockMode.Locked;

            inventario.SetActive(false);

            if (tienda.activeInHierarchy == true)
            {
                tienda.SetActive(false);
                abrir.SetActive(true);
                cerrar.SetActive(false);
            }
        }

        // ACTIVAR EL CURSOR

        if (Input.GetKeyDown(KeyCode.E))
        {

            Cursor.lockState = CursorLockMode.None;
        }

        // MOVIMIENTO

        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);
        
        // CORRER CON SHIFT IZQUIERDO

        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Vertical") : 0;
        float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Horizontal") : 0;
        float movementDirectionY = moveDirection.y;
        moveDirection = (forward * curSpeedX) + (right * curSpeedY);

        if (Input.GetButton("Jump") && canMove && characterController.isGrounded)
        {
            moveDirection.y = jumpSpeed;
        }
        else
        {
            moveDirection.y = movementDirectionY;
        }

        //si toca el suelo
        
        if (!characterController.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        // Mover el controlador
        characterController.Move(moveDirection * Time.deltaTime);

        // Rotación del jugador y la cam
        if (canMove)
        {
            rotationX += -Input.GetAxis("Mouse Y") * lookSpeed;
            rotationX = Mathf.Clamp(rotationX, -lookXLimit, lookXLimit);
            playerCamera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
            transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeed, 0);
        }
    }
}