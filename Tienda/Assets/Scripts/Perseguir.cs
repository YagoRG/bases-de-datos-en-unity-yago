﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perseguir : MonoBehaviour
{
    
    private GameObject wayPoint;
    private Vector3 wayPointPos;
    
    //Velocidad de los enemigos

    private float speed = 60.0f;
    void Start()
    {
        //perseguir objeto con este nombre

        wayPoint = GameObject.Find("FPSPlayer");
    }

    void Update()
    {
        //la posición del objetivo se convierte en el nuevo waypoint
        wayPointPos = new Vector3(wayPoint.transform.position.x, transform.position.y, wayPoint.transform.position.z);
        //se desplaza hacia el objetivo
        transform.position = Vector3.MoveTowards(transform.position, wayPointPos, speed * Time.deltaTime);
    }
}
